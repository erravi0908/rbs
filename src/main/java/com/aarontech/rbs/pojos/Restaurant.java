
package com.aarontech.rbs.pojos;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"restaurantCode",
"ownerName",
"displayName",
"totalTables",
"openTime",
"closeTime",
"established",
"gstNo",
"address"
})
@Document(collection = "Restaurant")
public class Restaurant {

@JsonProperty("restaurantCode")
private String restaurantCode;
@JsonProperty("ownerName")
private String ownerName;
@JsonProperty("displayName")
private String displayName;
@JsonProperty("totalTables")
private Double totalTables;
@JsonProperty("openTime")
private String openTime;
@JsonProperty("closeTime")
private String closeTime;
@JsonProperty("established")
private String established;
@JsonProperty("gstNo")
private String gstNo;
@JsonProperty("address")
private Address address;

@JsonProperty("restaurantCode")
public String getRestaurantCode() {
return restaurantCode;
}

@JsonProperty("restaurantCode")
public void setRestaurantCode(String restaurantCode) {
this.restaurantCode = restaurantCode;
}

@JsonProperty("ownerName")
public String getOwnerName() {
return ownerName;
}

@JsonProperty("ownerName")
public void setOwnerName(String ownerName) {
this.ownerName = ownerName;
}

@JsonProperty("displayName")
public String getDisplayName() {
return displayName;
}

@JsonProperty("displayName")
public void setDisplayName(String displayName) {
this.displayName = displayName;
}

@JsonProperty("totalTables")
public Double getTotalTables() {
return totalTables;
}

@JsonProperty("totalTables")
public void setTotalTables(Double totalTables) {
this.totalTables = totalTables;
}

@JsonProperty("openTime")
public String getOpenTime() {
return openTime;
}

@JsonProperty("openTime")
public void setOpenTime(String openTime) {
this.openTime = openTime;
}

@JsonProperty("closeTime")
public String getCloseTime() {
return closeTime;
}

@JsonProperty("closeTime")
public void setCloseTime(String closeTime) {
this.closeTime = closeTime;
}

@JsonProperty("established")
public String getEstablished() {
return established;
}

@JsonProperty("established")
public void setEstablished(String established) {
this.established = established;
}

@JsonProperty("gstNo")
public String getGstNo() {
return gstNo;
}

@JsonProperty("gstNo")
public void setGstNo(String gstNo) {
this.gstNo = gstNo;
}

@JsonProperty("address")
public Address getAddress() {
return address;
}

@JsonProperty("address")
public void setAddress(Address address) {
this.address = address;
}

}