package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"category",
"totalAvailable",
"tableCodes"
})
public class SixOccupancyTable {

@JsonProperty("category")
private String category;
@JsonProperty("totalAvailable")
private Double totalAvailable;
@JsonProperty("tableCodes")
private List<String> tableCodes = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("category")
public String getCategory() {
return category;
}

@JsonProperty("category")
public void setCategory(String category) {
this.category = category;
}

@JsonProperty("totalAvailable")
public Double getTotalAvailable() {
return totalAvailable;
}

@JsonProperty("totalAvailable")
public void setTotalAvailable(Double totalAvailable) {
this.totalAvailable = totalAvailable;
}

@JsonProperty("tableCodes")
public List<String> getTableCodes() {
return tableCodes;
}

@JsonProperty("tableCodes")
public void setTableCodes(List<String> tableCodes) {
this.tableCodes = tableCodes;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}

