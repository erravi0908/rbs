package com.aarontech.rbs.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"dishName",
"dishCode",
"cost",
"category"
})
public class Dish {

@JsonProperty("dishName")
private String dishName;
@JsonProperty("dishCode")
private Integer dishCode;
@JsonProperty("cost")
private Boolean cost;
@JsonProperty("category")
private String category;

@JsonProperty("dishName")
public String getDishName() {
return dishName;
}

@JsonProperty("dishName")
public void setDishName(String dishName) {
this.dishName = dishName;
}

@JsonProperty("dishCode")
public Integer getDishCode() {
return dishCode;
}

@JsonProperty("dishCode")
public void setDishCode(Integer dishCode) {
this.dishCode = dishCode;
}

@JsonProperty("cost")
public Boolean getCost() {
return cost;
}

@JsonProperty("cost")
public void setCost(Boolean cost) {
this.cost = cost;
}

@JsonProperty("category")
public String getCategory() {
return category;
}

@JsonProperty("category")
public void setCategory(String category) {
this.category = category;
}

}