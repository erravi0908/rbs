package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"nonVegStaters",
"nonVegMains"
})
public class NonVegCatgories {

@JsonProperty("nonVegStaters")
private List<NonVegStater> nonVegStaters = null;
@JsonProperty("nonVegMains")
private List<NonVegMain> nonVegMains = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("nonVegStaters")
public List<NonVegStater> getNonVegStaters() {
return nonVegStaters;
}

@JsonProperty("nonVegStaters")
public void setNonVegStaters(List<NonVegStater> nonVegStaters) {
this.nonVegStaters = nonVegStaters;
}

@JsonProperty("nonVegMains")
public List<NonVegMain> getNonVegMains() {
return nonVegMains;
}

@JsonProperty("nonVegMains")
public void setNonVegMains(List<NonVegMain> nonVegMains) {
this.nonVegMains = nonVegMains;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
	return "NonVegCatgories [nonVegStaters=" + nonVegStaters + ", nonVegMains=" + nonVegMains + "]";
}


}

