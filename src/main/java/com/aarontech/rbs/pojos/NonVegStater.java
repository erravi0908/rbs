package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"dishName",
"cost",
"dishCode"
})
public class NonVegStater {

@JsonProperty("dishName")
private String dishName;
@JsonProperty("cost")
private Double cost;
@JsonProperty("dishCode")
private String dishCode;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("dishName")
public String getDishName() {
return dishName;
}

@JsonProperty("dishName")
public void setDishName(String dishName) {
this.dishName = dishName;
}

@JsonProperty("cost")
public Double getCost() {
return cost;
}

@JsonProperty("cost")
public void setCost(Double cost) {
this.cost = cost;
}

@JsonProperty("dishCode")
public String getDishCode() {
return dishCode;
}

@JsonProperty("dishCode")
public void setDishCode(String dishCode) {
this.dishCode = dishCode;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
	return "NonVegStater [dishName=" + dishName + ", cost=" + cost + ", dishCode=" + dishCode + "]";
}


}

