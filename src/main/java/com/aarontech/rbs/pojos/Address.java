
package com.aarontech.rbs.pojos;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"addressLine1",
"addressLine2",
"city",
"state",
"country"
})
@Document
public class Address {

@JsonProperty("addressLine1")
private String addressLine1;
@JsonProperty("addressLine2")
private String addressLine2;
@JsonProperty("city")
private String city;
@JsonProperty("state")
private String state;
@JsonProperty("country")
private String country;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("addressLine1")
public String getAddressLine1() {
return addressLine1;
}

@JsonProperty("addressLine1")
public void setAddressLine1(String addressLine1) {
this.addressLine1 = addressLine1;
}

@JsonProperty("addressLine2")
public String getAddressLine2() {
return addressLine2;
}

@JsonProperty("addressLine2")
public void setAddressLine2(String addressLine2) {
this.addressLine2 = addressLine2;
}

@JsonProperty("city")
public String getCity() {
return city;
}

@JsonProperty("city")
public void setCity(String city) {
this.city = city;
}

@JsonProperty("state")
public String getState() {
return state;
}

@JsonProperty("state")
public void setState(String state) {
this.state = state;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
	return "Address [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", state="
			+ state + ", country=" + country + "]";
}



}

