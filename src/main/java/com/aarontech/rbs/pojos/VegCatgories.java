package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"vegStaters",
"vegMains"
})
public class VegCatgories {

@JsonProperty("vegStaters")
private List<VegStater> vegStaters = null;
@JsonProperty("vegMains")
private List<VegMain> vegMains = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("vegStaters")
public List<VegStater> getVegStaters() {
return vegStaters;
}

@JsonProperty("vegStaters")
public void setVegStaters(List<VegStater> vegStaters) {
this.vegStaters = vegStaters;
}

@JsonProperty("vegMains")
public List<VegMain> getVegMains() {
return vegMains;
}

@JsonProperty("vegMains")
public void setVegMains(List<VegMain> vegMains) {
this.vegMains = vegMains;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
	return "VegCatgories [vegStaters=" + vegStaters + ", vegMains=" + vegMains + "]";
}


}

