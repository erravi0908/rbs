package com.aarontech.rbs.pojos;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"menuCode",
"menuDisplayCode",
"restaurantCode",
"vegCatgories",
"nonVegCatgories"
})
@Document(collection = "Menu")
public class Menu {

@JsonProperty("menuCode")
private String menuCode;
@JsonProperty("menuDisplayCode")
private String menuDisplayCode;
@JsonProperty("restaurantCode")
private String restaurantCode;
@JsonProperty("vegCatgories")
private VegCatgories vegCatgories;
@JsonProperty("nonVegCatgories")
private NonVegCatgories nonVegCatgories;

@JsonProperty("menuCode")
public String getMenuCode() {
return menuCode;
}

@JsonProperty("menuCode")
public void setMenuCode(String menuCode) {
this.menuCode = menuCode;
}

@JsonProperty("menuDisplayCode")
public String getMenuDisplayCode() {
return menuDisplayCode;
}

@JsonProperty("menuDisplayCode")
public void setMenuDisplayCode(String menuDisplayCode) {
this.menuDisplayCode = menuDisplayCode;
}

@JsonProperty("restaurantCode")
public String getRestaurantCode() {
return restaurantCode;
}

@JsonProperty("restaurantCode")
public void setRestaurantCode(String restaurantCode) {
this.restaurantCode = restaurantCode;
}

@JsonProperty("vegCatgories")
public VegCatgories getVegCatgories() {
return vegCatgories;
}

@JsonProperty("vegCatgories")
public void setVegCatgories(VegCatgories vegCatgories) {
this.vegCatgories = vegCatgories;
}

@JsonProperty("nonVegCatgories")
public NonVegCatgories getNonVegCatgories() {
return nonVegCatgories;
}

@JsonProperty("nonVegCatgories")
public void setNonVegCatgories(NonVegCatgories nonVegCatgories) {
this.nonVegCatgories = nonVegCatgories;
}




@Override
public String toString() {
	return "Menu [menuCode=" + menuCode + ", menuDisplayCode=" + menuDisplayCode + ", restaurantCode=" + restaurantCode
			+ ", vegCatgories=" + vegCatgories + ", nonVegCatgories=" + nonVegCatgories + "]";
}





}