package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"customerName",
"mobileNo",
"orderStatus",
"tableNo",
"orderItems"
})
public class Orders {

@JsonProperty("customerName")
private String customerName;
@JsonProperty("mobileNo")
private String mobileNo;
@JsonProperty("orderStatus")
private String orderStatus;
@JsonProperty("tableNo")
private String tableNo;
@JsonProperty("orderItems")
private List<OrderItem> orderItems = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("customerName")
public String getCustomerName() {
return customerName;
}

@JsonProperty("customerName")
public void setCustomerName(String customerName) {
this.customerName = customerName;
}

@JsonProperty("mobileNo")
public String getMobileNo() {
return mobileNo;
}

@JsonProperty("mobileNo")
public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

@JsonProperty("orderStatus")
public String getOrderStatus() {
return orderStatus;
}

@JsonProperty("orderStatus")
public void setOrderStatus(String orderStatus) {
this.orderStatus = orderStatus;
}

@JsonProperty("tableNo")
public String getTableNo() {
return tableNo;
}

@JsonProperty("tableNo")
public void setTableNo(String tableNo) {
this.tableNo = tableNo;
}

@JsonProperty("orderItems")
public List<OrderItem> getOrderItems() {
return orderItems;
}

@JsonProperty("orderItems")
public void setOrderItems(List<OrderItem> orderItems) {
this.orderItems = orderItems;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}