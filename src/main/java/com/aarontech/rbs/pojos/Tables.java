package com.aarontech.rbs.pojos;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"TwoOccupancyTable",
"FourOccupancyTable",
"SixOccupancyTable"
})
public class Tables {

@JsonProperty("TwoOccupancyTable")
private TwoOccupancyTable twoOccupancyTable;
@JsonProperty("FourOccupancyTable")
private FourOccupancyTable fourOccupancyTable;
@JsonProperty("SixOccupancyTable")
private SixOccupancyTable sixOccupancyTable;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("TwoOccupancyTable")
public TwoOccupancyTable getTwoOccupancyTable() {
return twoOccupancyTable;
}

@JsonProperty("TwoOccupancyTable")
public void setTwoOccupancyTable(TwoOccupancyTable twoOccupancyTable) {
this.twoOccupancyTable = twoOccupancyTable;
}

@JsonProperty("FourOccupancyTable")
public FourOccupancyTable getFourOccupancyTable() {
return fourOccupancyTable;
}

@JsonProperty("FourOccupancyTable")
public void setFourOccupancyTable(FourOccupancyTable fourOccupancyTable) {
this.fourOccupancyTable = fourOccupancyTable;
}

@JsonProperty("SixOccupancyTable")
public SixOccupancyTable getSixOccupancyTable() {
return sixOccupancyTable;
}

@JsonProperty("SixOccupancyTable")
public void setSixOccupancyTable(SixOccupancyTable sixOccupancyTable) {
this.sixOccupancyTable = sixOccupancyTable;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}

