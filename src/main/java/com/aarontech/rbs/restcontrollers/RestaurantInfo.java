package com.aarontech.rbs.restcontrollers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.aarontech.rbs.common.utils.PojoToJsonConverterService;
import com.aarontech.rbs.pojos.Restaurant;
import com.aarontech.rbs.repository.services.RestaurantRepositoryService;


@RestController
public class RestaurantInfo {

	@Autowired
	RestaurantRepositoryService restaurantService;
	@Autowired
	PojoToJsonConverterService pojoToJsonConverterService;

	/*
	 * @GetMapping(path = "/hello") public String hello() { return "Hello"; }
	 */

	@GetMapping(path = "/restaurant/{displayName}")
	public String findByRestaurantDisplayName(@PathVariable String displayName) {


		Restaurant restaurant = restaurantService.findByDisplayName(displayName);

		return pojoToJsonConverterService.pojoToJson(restaurant);
	}

	@GetMapping(path = "/restaurant/all")
	public String listAllRestaurant() {

		List<Restaurant> restaurantList = restaurantService.findAll();

		return pojoToJsonConverterService.pojoToJson(restaurantList);

	}

}