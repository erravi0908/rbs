package com.aarontech.rbs.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aarontech.rbs.common.utils.PojoToJsonConverterService;
import com.aarontech.rbs.pojos.Menu;
import com.aarontech.rbs.repository.services.MenuRepositoryService;

@RestController
public class MenuController {

	@Autowired
	MenuRepositoryService menuRepositoryService;
	@Autowired
	PojoToJsonConverterService pojoToJsonConverterService;

	@GetMapping(path = "/menu/all")
	public String getAllMenu() {

		List<Menu> menuList = menuRepositoryService.findAll();

		return pojoToJsonConverterService.pojoToJson(menuList);

	}

	@GetMapping(path = "/find/menu/{restaurantCode}/{menuCode}")
	public String findMenu(@PathVariable String restaurantCode, @PathVariable String menuCode) {

		Menu menuSaved = menuRepositoryService.findByMenuCode(menuCode);

		return pojoToJsonConverterService.pojoToJson(menuSaved);
	}

	@PostMapping(path = "/create/menu", consumes = "application/json", produces = "application/json")
	public String createMenu(@RequestBody Menu menu) {

		Menu menuSaved = menuRepositoryService.save(menu);

		return pojoToJsonConverterService.pojoToJson(menuSaved);

	}

	@PutMapping(path = "/update/menu/{restaurantCode}/{menuCode}", consumes = "application/json", produces = "application/json")
	public String updateMenu(@PathVariable String restaurantCode, @PathVariable String menuCode,
			@RequestBody Menu menu) {

		Menu menuSaved = menuRepositoryService.save(menu);

		return pojoToJsonConverterService.pojoToJson(menuSaved);

	}

	@DeleteMapping(path = "/delete/menu/{menuCode}", produces = "application/json")
	public String deleteMenu(String menuCode) {

		Menu menuSaved = menuRepositoryService.deleteByMenuCode(menuCode);

		return  pojoToJsonConverterService.pojoToJson(menuSaved);
	}

}
