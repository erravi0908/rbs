
package com.aarontech.rbs.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
public class AppConfig {

	public @Bean MongoClient mongoClient() {

		//String url = "";

		// MongoClient mongoClient = MongoClients.create(
		// "mongodb://rbs:rbs@cluster0-shard-00-01.jkzt3.mongodb.net/rbs?ssl=true&authSource=admin&authMechanism=SCRAM-SHA-1&retryWrites=true&w=majority&connectTimeoutMS=120000&socketTimeoutMS=1000000");

		ConnectionString connectionString = new ConnectionString(
				"mongodb://rbs:rbs@cluster0-shard-00-01.jkzt3.mongodb.net/rbs?serverSelectionTimeoutMS=120000&ssl=true&authSource=admin&authMechanism=SCRAM-SHA-1&retryWrites=true&w=majority&connectTimeoutMS=120000&socketTimeoutMS=1000000");

		MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(connectionString).build();

		MongoClient mongoClient = MongoClients.create(settings);

		return mongoClient;

	}

	public @Bean MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoClient(), "rbs");

	}
	
	public @Bean ObjectMapper objectMapper()
	{
		return new ObjectMapper();
	}
	
	
}
