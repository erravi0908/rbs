package com.aarontech.rbs.common.utils;


public interface PojoToJsonConverterService {
	
	public String pojoToJson(Object object);

}
