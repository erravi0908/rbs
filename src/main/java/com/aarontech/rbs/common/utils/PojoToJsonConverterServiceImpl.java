package com.aarontech.rbs.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PojoToJsonConverterServiceImpl implements PojoToJsonConverterService{

	
	@Autowired
	ObjectMapper objectMapper;
	
	@Override
	public String pojoToJson(Object object) {
		// TODO Auto-generated method stub
		
		String result = "{}";

		ObjectMapper om = new ObjectMapper();

		try {
			if (object != null) {
				result = om.writeValueAsString(object);
			}

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	

}
