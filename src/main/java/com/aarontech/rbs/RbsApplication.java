package com.aarontech.rbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;



@SpringBootApplication(scanBasePackages = "com.aarontech.rbs.*")
@EnableMongoRepositories(basePackages ="com.aarontech.rbs.repository")
public class RbsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RbsApplication.class, args);
	}

}
