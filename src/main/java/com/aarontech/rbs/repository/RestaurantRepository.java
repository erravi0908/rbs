package com.aarontech.rbs.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.aarontech.rbs.pojos.Restaurant;

public interface RestaurantRepository extends MongoRepository<Restaurant, Object> {

	public Restaurant findByOwnerName(String ownerName);

	public List<Restaurant> findAll();

	public Restaurant findByDisplayName(String displayName);

}
