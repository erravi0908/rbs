package com.aarontech.rbs.repository.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aarontech.rbs.pojos.Restaurant;
import com.aarontech.rbs.repository.RestaurantRepository;

@Service
public class RestaurantRepositoryServiceImpl implements RestaurantRepositoryService {
	
	@Autowired
	RestaurantRepository restaurantRepository;

	@Override
	public Restaurant findByOwnerName(String ownerName) {
		// TODO Auto-generated method stub
		return restaurantRepository.findByOwnerName(ownerName);
	}

	

	@Override
	public List<Restaurant> findAll() {
		// TODO Auto-generated method stub
		return restaurantRepository.findAll();
	}



	@Override
	public Restaurant findByDisplayName(String displayName) {
		// TODO Auto-generated method stub
		return restaurantRepository.findByDisplayName(displayName);
	}

	
}
