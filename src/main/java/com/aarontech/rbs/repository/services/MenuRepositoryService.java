package com.aarontech.rbs.repository.services;

import java.util.List;

import com.aarontech.rbs.pojos.Menu;

public interface MenuRepositoryService {

	public List<Menu> findAll() ;
	
	public Menu save(Menu menu);
	
	public Menu findByMenuCode(String menuCode);
	
	public Menu deleteByMenuCode(String menuCode);
	
	
}
