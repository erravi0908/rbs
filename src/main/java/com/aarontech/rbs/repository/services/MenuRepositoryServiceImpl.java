package com.aarontech.rbs.repository.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.aarontech.rbs.pojos.Menu;
import com.aarontech.rbs.repository.MenuRepository;

@Service
public class MenuRepositoryServiceImpl implements MenuRepositoryService {

	@Autowired
	MenuRepository menuRepository;
	@Autowired
	MongoOperations mongoOps;

	@Override
	public List<Menu> findAll() {
		// TODO Auto-generated method stub
		return menuRepository.findAll();
	}

	@Override
	public Menu save(Menu menu) {
		// TODO Auto-generated method stub

		Menu menuSaved = mongoOps.save(menu);
		return menuSaved;
	}

	@Override
	public Menu findByMenuCode(String menuCode) {
		// TODO Auto-generated method stub
		return menuRepository.findByMenuCode(menuCode);
	}

	@Override
	public Menu deleteByMenuCode(String menuCode) {
		// TODO Auto-generated method stub
		 Query query = new Query();
	     query.addCriteria(Criteria.where("menuCode").is(menuCode));
	     Menu menu = mongoOps.findAndRemove(query, Menu.class);
		return menu;
	}

}
