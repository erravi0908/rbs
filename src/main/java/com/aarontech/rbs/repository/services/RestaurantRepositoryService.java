package com.aarontech.rbs.repository.services;


import java.util.List;

import com.aarontech.rbs.pojos.Restaurant;


public interface  RestaurantRepositoryService{

	public Restaurant findByOwnerName(String ownerName);
	
	public List<Restaurant> findAll();
	//displayName
	public Restaurant findByDisplayName(String displayName);
	

	
}
