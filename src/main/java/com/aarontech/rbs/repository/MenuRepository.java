package com.aarontech.rbs.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.aarontech.rbs.pojos.Menu;

public interface MenuRepository extends MongoRepository<Menu, Object>{

	public List<Menu> findAll() ;
	
	public Menu findByMenuCode(String menuCode);

	
	
	

}
